function RenderNews() {
    ////////////////////////////DESTAQUE /////////////////////////
    var NewAUX_Destaque = DestaqueNews[0];
    var ImgNews = document.createElement('img');
    ImgNews.setAttribute('src', NewAUX_Destaque.urlToImage);
    ImgNews.setAttribute('target', '_blank');
    ImgNews.setAttribute('href', NewAUX_Destaque.url);

    var TitleNews = document.createElement('strong');
    var TitleText = document.createTextNode(NewAUX_Destaque.title);
    TitleNews.appendChild(TitleText);

    var urlNews = document.createElement('a');
    urlNews.setAttribute('target', '_blank');
    urlNews.setAttribute('href', NewAUX_Destaque.url);
    urlNews.appendChild(ImgNews);
    urlNews.appendChild(TitleNews);

    var listEl = document.createElement('li');

    listEl.appendChild(urlNews);
    PrincipalDestaque.appendChild(listEl);

    ////////////////////////////DESTAQUE-SECUNDARIO /////////////////////////
    for (var i = 1; i < 3; i++) {
        var NewAUX_Destaque = DestaqueNews[i];
        var ImgNews = document.createElement('img');
        ImgNews.setAttribute('src', NewAUX_Destaque.urlToImage);
        ImgNews.setAttribute('target', '_blank');
        ImgNews.setAttribute('href', NewAUX_Destaque.url);

        var TitleNews = document.createElement('strong');
        var TitleText = document.createTextNode(NewAUX_Destaque.title);
        TitleNews.appendChild(TitleText);

        var urlNews = document.createElement('a');
        urlNews.setAttribute('target', '_blank');
        urlNews.setAttribute('href', NewAUX_Destaque.url);
        urlNews.appendChild(ImgNews);
        urlNews.appendChild(TitleNews);

        var listEl = document.createElement('li');
        listEl.appendChild(urlNews);

        SecundarioDestaque.appendChild(listEl);
    }


    for (var i = 4; i < 6; i++) {
        var NewAUX_Destaque = DestaqueNews[i];
        var ImgNews = document.createElement('img');
        ImgNews.setAttribute('src', NewAUX_Destaque.urlToImage);
        ImgNews.setAttribute('target', '_blank');
        ImgNews.setAttribute('href', NewAUX_Destaque.url);

        var TitleNews = document.createElement('strong');
        var TitleText = document.createTextNode(NewAUX_Destaque.title);
        TitleNews.appendChild(TitleText);

        var urlNews = document.createElement('a');
        urlNews.setAttribute('target', '_blank');
        urlNews.setAttribute('href', NewAUX_Destaque.url);
        urlNews.appendChild(ImgNews);
        urlNews.appendChild(TitleNews);

        var listEl = document.createElement('li');
        listEl.appendChild(urlNews);

        SecundarioDestaque_2.appendChild(listEl);
    }

    ////////////////////////////DESTAQUE-3 /////////////////////////
    var NewAUX_Destaque = DestaqueNews[8];
    var ImgNews = document.createElement('img');
    ImgNews.setAttribute('src', NewAUX_Destaque.urlToImage);
    ImgNews.setAttribute('target', '_blank');
    ImgNews.setAttribute('href', NewAUX_Destaque.url);

    var TitleNews = document.createElement('strong');
    var TitleText = document.createTextNode(NewAUX_Destaque.title);
    TitleNews.appendChild(TitleText);

    var urlNews = document.createElement('a');
    urlNews.setAttribute('target', '_blank');
    urlNews.setAttribute('href', NewAUX_Destaque.url);
    urlNews.appendChild(ImgNews);
    urlNews.appendChild(TitleNews);

    var listEl = document.createElement('li');
    listEl.appendChild(urlNews);

    Destaque3.appendChild(listEl);

    var NewAUX_Destaque = DestaqueNews[9];
    var ImgNews = document.createElement('img');
    ImgNews.setAttribute('src', NewAUX_Destaque.urlToImage);
    ImgNews.setAttribute('target', '_blank');
    ImgNews.setAttribute('href', NewAUX_Destaque.url);

    var TitleNews = document.createElement('strong');
    var TitleText = document.createTextNode(NewAUX_Destaque.title);
    TitleNews.appendChild(TitleText);

    var urlNews = document.createElement('a');
    urlNews.setAttribute('target', '_blank');
    urlNews.setAttribute('href', NewAUX_Destaque.url);
    urlNews.appendChild(ImgNews);
    urlNews.appendChild(TitleNews);

    var listEl = document.createElement('li');
    listEl.appendChild(urlNews);

    Destaque3_2.appendChild(listEl);
}

