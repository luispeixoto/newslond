var DestaqueNews = [];

NewsElemnts("https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.eurogamer.pt%2F%3Fformat%3Drss");

function NewsElemnts(urlStr) {

    $.ajax({
        url: urlStr,
        type: "get",
        dataType: "json",
        success: function (data) {
            for (NewAUX of data.items) {
                DestaqueNews.push({
                    title: NewAUX.title,
                    description: NewAUX.description,
                    urlToImage: NewAUX.thumbnail,
                    url: NewAUX.link
                });
            }
            RenderNews();
        },
        error: function (erro) {
        }
    });
}
